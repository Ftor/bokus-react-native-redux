import React, {Component} from 'react';
import {BackHandler, Text, TouchableHighlight, ScrollView, SafeAreaView, StyleSheet} from 'react-native';
import {Grid, Row} from 'react-native-easy-grid';
import HeaderTitle from '../custom_components/header_title';
import HeaderRight from '../custom_components/header_right';

export default class Description extends Component {

    static navigationOptions = ({navigation})=>  {
        return{
            headerTitle: () => <HeaderTitle title={navigation.getParam('title', 'График')}/>,
            headerRight: () => <HeaderRight user={navigation.getParam('login', 'User')}/>,
            headerStyle: {
                backgroundColor: 'crimson',
            },
            headerTintColor: '#fff',
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({ login: this.props.login });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        const currentScreen = this.props.navigation.state.routeName;
        if (currentScreen === "Description") {
            this.props.navigation.goBack();
            return true;
        } else {
            return false;
        }
    };

    handleLoginResetButton = () => {
        this.props.clearAuth();
        this.props.navigation.navigate('Home')
    };


    render() {
        const {navigation} = this.props;
        const title = navigation.getParam('title','no-title');
        const description = navigation.getParam('description','no-description');
        return (
            <Grid>
                <Row style={{height: 60}}>
                    <Text
                        style={styles.textTitle}>
                        {title}
                    </Text>
                </Row>
                <SafeAreaView style={{flex:1}}>
                    <ScrollView>
                <Row>
                    <Text style={styles.textDescription}>
                        {description}
                    </Text>
                </Row>
                    </ScrollView>
                </SafeAreaView>
                <Row style={styles.buttonsContainer}>
                    <TouchableHighlight
                        style={styles.buttonBackContainer}
                        onPress={() => this.handleBackButton()}
                    >
                        <Text style={styles.buttonBackText}>Назад</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        style={styles.buttonExitContainer}
                        onPress={() => this.handleLoginResetButton()}
                    >
                        <Text style={styles.buttonExitText}>Выйти из аккаунта</Text>
                    </TouchableHighlight>
                </Row>
            </Grid>
        );
    }
}

const styles = StyleSheet.create({
    textTitle:{
        textAlignVertical: 'center',
        paddingHorizontal: 20,
        fontSize: 17,
        fontWeight: 'bold'
    },
    textDescription:{
        paddingHorizontal: 20,
        fontSize: 17
    },
    buttonsContainer:{
        height: 40,
        justifyContent: 'space-between'
    },
    buttonBackContainer:{
        borderWidth: 1,
        borderColor: 'crimson',
        width: '50%',
        backgroundColor: 'white'
    },
    buttonBackText: {
        flex: 1,
        textAlign: 'center',
        color: 'crimson',
        textAlignVertical: 'center',
        fontSize: 17,
        fontWeight: 'bold'
    },
    buttonExitContainer:{
        borderWidth: 1,
        borderColor: 'crimson',
        width: '50%',
        backgroundColor: 'crimson'
    },
    buttonExitText: {
        flex: 1,
        textAlign: 'center',
        color: 'white',
        textAlignVertical: 'center',
        fontSize: 17,
        fontWeight: 'bold'
    }

});