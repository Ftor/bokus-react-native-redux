import React, {Component} from 'react';
import {BackHandler, FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity} from 'react-native';
import HeaderTitle from '../custom_components/header_title';
import HeaderRight from '../custom_components/header_right';
import {DATA} from '../testData'

export default class List extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: ()=> null,
            headerTitle: () => <HeaderTitle title='Список'/>,
            headerRight: () => <HeaderRight user={navigation.getParam('login', 'User')}/>,
            headerStyle: {
                backgroundColor: 'crimson',
            },
            headerTintColor: '#fff',
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({login: this.props.login});
        BackHandler.addEventListener('hardwareBackPress', function () {
            return true;
        });
    }

    render() {
        const {navigate} = this.props.navigation;

        function Item({title, description}) {
            return (
                <TouchableOpacity
                    style={styles.listButton}
                    onPress={() => navigate('Description', {title, description})}
                >
                    <Text>{title}</Text>
                </TouchableOpacity>
            );
        }

        return (
            <SafeAreaView style={styles.container}>
                <FlatList
                    style={styles.listContainer}
                    data={DATA}
                    renderItem={({item}) => <Item title={item.title} description={item.description}/>}
                    keyExtractor={item => item.id.toString()}
                />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    listContainer: {
        alignSelf: 'stretch',
        flex: 1
    },
    listButton:{
        padding: 10,
        margin: 10,
        borderWidth: 1,
        borderColor: 'gray'
    }
});