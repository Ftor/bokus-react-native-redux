import React, {Component} from 'react';
import {KeyboardAvoidingView, StyleSheet, Text, TextInput, TouchableHighlight, View} from 'react-native';
import {Col, Grid, Row} from 'react-native-easy-grid';

export default class Home extends Component {
    static navigationOptions = {
        title: 'Вход в личный кабинет',
        headerStyle: {
            backgroundColor: 'crimson',
        },
        headerTintColor: '#fff'

    };

    _onLoginChange(text) {
        this.props.setLogin(text);
    }

    _onPasswordChange(text) {
        this.props.setPassword(text);
    }

    render() {
        const {navigate} = this.props.navigation;
        const loremipsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ';
        return (
            <KeyboardAvoidingView style={styles.container}>
                <Grid>
                    <Col style={{flex: 2}}>
                        <View style={styles.mainTextContainer}>
                            <Text style={styles.mainText}>Вход</Text>
                        </View>
                        <View style={styles.descriptionContainer}>
                            <Text style={styles.descriptionText}>{loremipsum}</Text>
                        </View>
                        <View>
                            <TextInput placeholder='Логин'
                                       style={styles.input}
                                       onChangeText={(text) => this._onLoginChange(text)}
                                       value={this.props.login}
                            />
                            <TextInput placeholder='Пароль'
                                       style={styles.input}
                                       onChangeText={(text) => this._onPasswordChange(text)}
                                       value={this.props.password}
                                       secureTextEntry={true}
                            />
                        </View>
                    </Col>
                    <Row style={styles.buttonContainer}>
                        <TouchableHighlight
                            style={styles.button}
                            onPress={() => navigate('List')}
                        >
                            <Text style={styles.buttonText}>Войти</Text>
                        </TouchableHighlight>
                    </Row>
                </Grid>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    mainTextContainer:{
        flex: 1,
        justifyContent: 'flex-end',
        alignSelf: 'center'
    },
    mainText:{
        color: 'crimson',
        fontWeight: 'bold',
        fontSize: 40
    },
    descriptionContainer:{
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
        width: '70%'
    },
    descriptionText:{
        textAlign: 'center'
    },
    input: {
        alignSelf: 'center',
        borderColor: 'lightgray',
        borderBottomWidth: 1,
        width: '90%',
        paddingTop: 10
    },
    buttonContainer: {paddingTop: 20,
        flex: 1,
        justifyContent: 'center'
    },
    button: {
        height: 50,
        width: '70%',
        borderRadius: 8,
        borderWidth: 1,
        borderColor: 'crimson',
        backgroundColor: 'crimson'
    },
    buttonText: {
        flex: 1,
        textAlign: 'center',
        color: 'white',
        textAlignVertical: 'center',
        fontSize: 20,
    }
});
