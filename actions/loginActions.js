export const clearAuth = () => {
    return {
        type: 'CLEAR'
    }
};

export const setLogin = (recievedLogin) => {
    return {
        type:'SET_LOGIN',
        payload:recievedLogin
    }
};

export const setPassword = (recievedPassword) => {
    return {
        type:'SET_PASSWORD',
        payload:recievedPassword
    }
};