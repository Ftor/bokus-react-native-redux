import React, {Component} from "react";
import {View, Text } from 'react-native';
import { Icon } from 'native-base';

export default class HeaderRight extends Component {
    render() {
        return (
            <View style={{flexDirection:'row',flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color: '#fff', fontSize:20,paddingRight:10}}>
                    {this.props.user}
                </Text>
                <Icon
                    name='user'
                    type='FontAwesome'
                    style={{color:'#fff',paddingRight:20}}
                />
            </View>
        )
    }
}