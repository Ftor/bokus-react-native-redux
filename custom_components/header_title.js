import React, {Component} from "react";
import {View,Text} from 'react-native';

export default class HeaderTitle extends Component {
    render() {
        return (
            <View>
                <Text style={{color: '#fff', fontSize:20}}>{this.props.title}</Text>
            </View>
        )
    }
}