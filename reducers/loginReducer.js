const initialState = {
    login: 'anonymous',
    password: ''
};


export default (state = initialState, action) => {
    switch (action.type) {
        case 'CLEAR':
            return state ='';
        case 'SET_LOGIN':
            return {...state, login: action.payload};
        case 'SET_PASSWORD':
            return {...state, password: action.payload};
        default:
            return state;
    }
}