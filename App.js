import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {connect, Provider} from 'react-redux';
import {createStore} from 'redux';

import Home from './screens/HomeScreen';
import List from './screens/ListScreen';
import Description from './screens/DescriptionScreen';
import reducer from './reducers/loginReducer';
import {clearAuth,setLogin,setPassword} from "./actions";


const store = createStore(reducer);

let HomeContainer = connect(mapStateToProps,{clearAuth,setLogin,setPassword})(Home);
let DescriptionContainer = connect(mapStateToProps,{clearAuth,setLogin,setPassword})(Description);
let ListContainer = connect(mapStateToProps,{clearAuth,setLogin,setPassword})(List);

function mapStateToProps(state) {
    return {
        login: state.login,
        password: state.password,
    }
}

const RootStack = createStackNavigator({
        Home: {screen: HomeContainer},
        Description: {screen: DescriptionContainer},
        List: {screen: ListContainer}
    },
    {
        initialRouteName: 'Home',
    });

const Navigation= createAppContainer(RootStack);

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Navigation/>
            </Provider>
        );
    }
}


